<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('reports');
})->name('home');
Route::get('/reports', \App\Http\Controllers\TransactionReportController::class)->name('reports');
Route::post('/reports', \App\Http\Controllers\TransactionReportController::class)->name('reports.filter');
Route::get('/transactions', \App\Http\Controllers\TransactionListController::class)->name('transactions');
Route::post('/transactions', \App\Http\Controllers\TransactionListController::class)->name('transactions.filter');
Route::get('/transaction/{id}', \App\Http\Controllers\TransactionController::class)->name('transaction');
Route::get('/client/{id}', \App\Http\Controllers\ClientController::class)->name('client');
