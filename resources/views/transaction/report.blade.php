<x-layout>
    <x-slot:title>Reports</x-slot>
        <form class="row" method="POST" action="{{route('reports.filter')}}">
            @csrf <!-- {{ csrf_field() }} -->
            <div class="mb-3 col-6">
                <label for="from_date_id" class="form-label">From Date</label>
                <input type="text" class="form-control" id="from_date_id" name="fromDate" placeholder="YYYY-MM-DD" value="{{ request()->old('fromDate') ?: request()->get('fromDate') }}">
            </div>
            <div class="mb-3 col-6">
                <label for="from_date_id" class="form-label">To Date</label>
                <input type="text" class="form-control" id="from_date_id" name="toDate" placeholder="YYYY-MM-DD" value="{{ request()->old('toDate') ?: request()->get('toDate') }}">
            </div>

            <div class="mb-3 col-6">
                <label for="merchant_id" class="form-label">MerchantId</label>
                <input type="text" class="form-control" id="merchant_id" name="merchantId" placeholder="0" value="{{ request()->old('merchantId') ?: request()->get('merchantId') }}">
            </div>
            <div class="mb-3 col-6">
                <label for="from_date_id" class="form-label">Acquirer Id</label>
                <input type="text" class="form-control" id="from_date_id" name="acquirerId" placeholder="0" value="{{ request()->old('acquirerId') }}">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <div class="table-responsive">
            <table class="table table-striped">
            <thead>
                <tr>
                    <th>Count</th>
                    <th>Total</th>
                    <th>Currency</th>
                </tr>
            </thead>
            @foreach ($reports as $report)
                <tr>
                    <td>{{ $report['count'] }}</td>
                    <td>{{ $report['total'] }}</td>
                    <td>{{ $report['currency'] }}</td>
                </tr>
            @endforeach
            </table>
        </div>
</x-layout>
