<x-layout>
    <x-slot:title>Reports</x-slot>
        <div>
            <a href="javascript:history.back()">Go Back</a>
        </div>
        <h2>Customer Info</h2>
        <table class="table table-striped">
            @foreach($client['customerInfo'] as $key => $info)
                <tr>
                    <th>{{$key}}</th>
                    <td>{{$info}}</td>
                </tr>
            @endforeach
        </table>
</x-layout>
