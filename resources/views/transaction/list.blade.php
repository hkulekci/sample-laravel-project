<x-layout>
    <x-slot:title>Transactions</x-slot>
        <form class="row" method="POST" action="{{route('transactions.filter')}}">
            @csrf <!-- {{ csrf_field() }} -->
            <div class="mb-3 col-6">
                <label for="from_date_id" class="form-label">From Date</label>
                <input type="text" class="form-control" id="from_date_id" name="fromDate" placeholder="YYYY-MM-DD" value="{{ request()->old('fromDate') ?: request()->get('fromDate') }}">
            </div>
            <div class="mb-3 col-6">
                <label for="from_date_id" class="form-label">To Date</label>
                <input type="text" class="form-control" id="from_date_id" name="toDate" placeholder="YYYY-MM-DD" value="{{ request()->old('toDate') ?: request()->get('toDate') }}">
            </div>
            <div class="mb-3 col-4">
                <label for="statuses_id" class="form-label">Status</label>
                <select class="form-control" id="statuses_id" name="status">
                    <option value=""></option>
                    @foreach ($statuses as $status)
                        <option value="{{$status}}"{{ (request()->old('status') ?: request()->get('status')) === $status ? ' selected':'' }}>{{$status}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3 col-4">
                <label for="operations_id" class="form-label">Operation</label>
                <select class="form-control" id="operations_id" name="operation">
                    <option value=""></option>
                    @foreach ($operations as $status)
                        <option value="{{$status}}"{{ (request()->old('operation') ?: request()->get('status')) === $status ? ' selected':'' }}>{{$status}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3 col-4">
                <label for="payment_methods_id" class="form-label">Payment Method</label>
                <select class="form-control" id="payment_methods_id" name="paymentMethod">
                    <option value=""></option>
                    @foreach ($paymentMethods as $status)
                        <option value="{{$status}}"{{ (request()->old('paymentMethod') ?: request()->get('paymentMethod')) === $status ? ' selected':'' }}>{{$status}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3 col-6">
                <label for="merchant_id" class="form-label">MerchantId</label>
                <input type="text" class="form-control" id="merchant_id" name="merchantId" placeholder="0" value="{{ request()->old('merchantId') ?: request()->get('merchantId') }}">
            </div>
            <div class="mb-3 col-6">
                <label for="from_date_id" class="form-label">Acquirer Id</label>
                <input type="text" class="form-control" id="from_date_id" name="acquirerId" placeholder="0" value="{{ request()->old('acquirerId') ?: request()->get('acquirerId') }}">
            </div>
            <div class="mb-3 col-6">
                <label for="filter_fields_id" class="form-label">Filter Field</label>
                <select class="form-control" id="filter_fields_id" name="filterField">
                    <option value=""></option>
                    @foreach ($filterFields as $status)
                        <option value="{{$status}}"{{ (request()->old('filterField') ?: request()->get('filterField')) === $status ? ' selected':'' }}>{{$status}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3 col-6">
                <label for="from_date_id" class="form-label">Filter Value</label>
                <input type="text" class="form-control" id="from_date_id" name="filterValue" placeholder="" value="{{ request()->old('filterValue') ?: request()->get('filterValue') }}">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <div class="table-responsive">
            <table class="table table-striped">
            <thead>
                <tr>
                    <th>Transaction Id</th>
                    <th>Status</th>
                    <th>Operation</th>
                    <th>Message</th>
                    <th>Billing First Name</th>
                    <th>Billing Last Name</th>
                    <th>Merchant Name</th>
                    <th></th>
                </tr>
            </thead>
            @foreach ($transactions as $transaction)
                <tr>
                    <td>
                        <a href="{{route('transaction', $transaction['transaction']['merchant']['transactionId'])}}">
                        {{ $transaction['transaction']['merchant']['transactionId'] }}
                        </a>
                    </td>
                    <td>{{ $transaction['transaction']['merchant']['status'] }}</td>
                    <td>{{ $transaction['transaction']['merchant']['operation'] }}</td>
                    <td>{{ $transaction['transaction']['merchant']['message'] }}</td>
                    <td>{{ $transaction['customerInfo']['billingFirstName'] }}</td>
                    <td>{{ $transaction['customerInfo']['billingLastName'] }}</td>
                    <td>{{ $transaction['merchant']['name'] }}</td>
                    <td>
                        <a href="{{route('client', $transaction['transaction']['merchant']['transactionId'])}}">Get Client Info</a>
                    </td>
                </tr>
            @endforeach
            </table>
        </div>
</x-layout>
