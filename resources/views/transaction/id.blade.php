<x-layout>
    <x-slot:title>Reports</x-slot>
        <div>
            <a href="javascript:history.back()">Go Back</a>
        </div>
        <h2>FX > Merchant</h2>
        <table class="table table-striped">
        <thead>
            <tr>
                <th>Original Amount</th>
                <th>Original Currency</th>
            </tr>
        </thead>
        <tr>
            <td>{{$transaction['fx']['merchant']['originalAmount']}}</td>
            <td>{{$transaction['fx']['merchant']['originalCurrency']}}</td>
        </tr>
        </table>
        <h2><a href="{{route('client', $transaction['transaction']['merchant']['transactionId'])}}">Customer Info</a></h2>
        <table class="table table-striped">
            @foreach($transaction['customerInfo'] as $key => $info)
                <tr>
                    <th>{{$key}}</th>
                    <td>{{$info}}</td>
                </tr>
            @endforeach
        </table>

        <h2>Merchant</h2>
        <table class="table table-striped">
            <tr>
                <th>Name</th>
                <td>{{$transaction['merchant']['name']}}</td>
            </tr>
        </table>
        <h2>Transaction</h2>
        <table class="table table-striped">
            @foreach($transaction['transaction']['merchant'] as $key => $info)
                @if($key !== 'agent')
                <tr>
                    <th>{{$key}}</th>
                    <td>{{$info}}</td>
                </tr>
                @endif
            @endforeach
        </table>
</x-layout>
