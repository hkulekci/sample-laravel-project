<?php

namespace Tests\Unit\Services\Reporting;

use App\Services\Reporting\Response;
use GuzzleHttp\Psr7\Response as HttpResponse;
use GuzzleHttp\Psr7\Utils;
use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{
    public function test_response_initialize_class(): void
    {
        $httpResponse = new HttpResponse(
            200,
            [
                'content-type' => 'application/json'
            ],
            Utils::streamFor(json_encode(['foo' => 'bar']))
        );
        $response = new Response($httpResponse);

        $this->assertFalse($response->isFailed());
        $this->assertEquals('bar', $response['foo']);
    }

    public function test_client_initialize_request(): void
    {
        $httpResponse = new HttpResponse(
            418,
            [
                'content-type' => 'application/json'
            ],
            Utils::streamFor(json_encode(['foo' => 'bar']))
        );
        $response = new Response($httpResponse);

        $this->assertTrue($response->isFailed());
        $this->assertEquals('bar', $response['foo']);
    }

    public function test_client_initialize_request_and_response(): void
    {
        $httpResponse = new HttpResponse(
            510,
            [
                'content-type' => 'application/json'
            ],
            Utils::streamFor(json_encode(['foo' => 'bar']))
        );
        $response = new Response($httpResponse);

        $this->assertTrue($response->isFailed());
        $this->assertEquals('bar', $response['foo']);
    }
}
