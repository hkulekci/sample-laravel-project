<?php

namespace Tests\Unit\Services\Reporting;

use App\Services\Reporting\ApiClient;
use App\Services\Reporting\Client;
use App\Services\Reporting\Endpoints\Transaction;
use GuzzleHttp\Psr7\Response as HttpResponse;
use GuzzleHttp\Psr7\Utils;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

class ApiClientTest extends TestCase
{
    public function test_api_client_initialize_class(): void
    {
        $httpResponse = new HttpResponse(
            200,
            [
                'content-type' => 'application/json'
            ],
            Utils::streamFor(json_encode(['foo' => 'bar']))
        );

        $baseClient = \Mockery::mock(ClientInterface::class);

        $client = new ApiClient($baseClient);

        $this->assertInstanceOf(Client::class, $client);
    }

    public function test_api_client_check_response(): void
    {
        $httpResponse = new HttpResponse(
            200,
            [
                'content-type' => 'application/json'
            ],
            Utils::streamFor(json_encode(['foo' => 'bar']))
        );

        $baseClient = \Mockery::mock(ClientInterface::class);
        $baseClient->shouldReceive('sendRequest')
            ->andReturn(new HttpResponse(
                418,
                [
                    'content-type' => 'application/json'
                ],
                Utils::streamFor(json_encode(['foo' => 'bar']))
            ));
        $client = new ApiClient($baseClient);

        $response = $client->execute(new Transaction('test-id'));

        $this->assertEquals('bar', $response['foo']);
        $this->assertTrue($response->isFailed());
    }
}
