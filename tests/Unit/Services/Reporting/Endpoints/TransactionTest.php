<?php

namespace Tests\Unit\Services\Reporting\Endpoints;

use App\Services\Reporting\Endpoints\Endpoint;
use App\Services\Reporting\Endpoints\Transaction;
use PHPUnit\Framework\TestCase;

class TransactionTest extends TestCase
{
    public function test_transaction_initialize_class(): void
    {
        $client = new Transaction('transaction-id');

        $this->assertInstanceOf(Endpoint::class, $client);
    }

    public function test_transaction_initialize_request(): void
    {
        $client = new Transaction('transaction-id');
        $request = $client->createRequest();

        $this->assertEquals('transaction', $request->getUri()->getPath());
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode(["transactionId" => "transaction-id"]), $request->getBody()->getContents());
    }
}
