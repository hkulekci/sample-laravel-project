<?php

namespace Tests\Unit\Services\Reporting\Endpoints;

use App\Services\Reporting\Endpoints\Endpoint;
use App\Services\Reporting\Endpoints\TransactionReport;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class TransactionReportTest extends TestCase
{
    public function test_transaction_report_initialize_class(): void
    {
        $client = new TransactionReport(new Carbon('2023-01-01'), new Carbon('2023-02-02'));

        $this->assertInstanceOf(Endpoint::class, $client);
    }

    public function test_transaction_report_initialize_request(): void
    {
        $client = new TransactionReport(new Carbon('2023-01-01'), new Carbon('2023-02-02'));
        $request = $client->createRequest();

        $this->assertEquals('transactions/report', $request->getUri()->getPath());
        $this->assertEquals('POST', $request->getMethod());
    }

    public function test_transaction_report_initialize_request_with_data_1(): void
    {
        $client = new TransactionReport(new Carbon('2023-01-01'), new Carbon('2023-02-02'));
        $request = $client->createRequest();

        $this->assertEquals(json_encode([
            'fromDate' => '2023-01-01',
            'toDate' => '2023-02-02',
        ]), $request->getBody()->getContents());
    }

    public function test_transaction_report_initialize_request_with_data_2(): void
    {
        $client = new TransactionReport(new Carbon('2023-01-01'), new Carbon('2023-02-02'), 1, 1);
        $request = $client->createRequest();

        $this->assertEquals(json_encode([
            'fromDate' => '2023-01-01',
            'toDate' => '2023-02-02',
            'merchant' => 1,
            'acquirer' => 1
        ]), $request->getBody()->getContents());
    }
}
