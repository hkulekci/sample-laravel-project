<?php

namespace Tests\Unit\Services\Reporting\Endpoints;

use App\Exceptions\ValidationException;
use App\Services\Reporting\Endpoints\Endpoint;
use App\Services\Reporting\Endpoints\TransactionQuery;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Utils;
use PHPUnit\Framework\TestCase;

class TransactionQueryTest extends TestCase
{
    public function test_transaction_query_initialize_class(): void
    {
        $client = new TransactionQuery();

        $this->assertInstanceOf(Endpoint::class, $client);
    }

    public function test_transaction_query_initialize_request(): void
    {
        $client = new TransactionQuery();
        $request = $client->createRequest();

        $this->assertEquals('transaction/list', $request->getUri()->getPath());
        $this->assertEquals('POST', $request->getMethod());
    }

    public function test_transaction_query_initialize_request_with_data_1(): void
    {
        $client = new TransactionQuery(new Carbon('2023-01-01'), new Carbon('2023-02-02'));
        $request = $client->createRequest();

        $this->assertEquals(json_encode([
            'fromDate' => '2023-01-01',
            'toDate' => '2023-02-02',
        ]), $request->getBody()->getContents());
    }

    public function test_transaction_query_initialize_request_with_data_2(): void
    {
        $client = new TransactionQuery(new Carbon('2023-01-01'), new Carbon('2023-02-02'), 'APPROVED');
        $request = $client->createRequest();

        $this->assertEquals(json_encode([
            'fromDate' => '2023-01-01',
            'toDate' => '2023-02-02',
            'status' => 'APPROVED'
        ]), $request->getBody()->getContents());
    }

    public function test_transaction_query_initialize_request_with_data_3(): void
    {
        $client = new TransactionQuery(
            new Carbon('2023-01-01'),
            new Carbon('2023-02-02'),
            'APPROVED',
            'DIRECT',
            1,
            2,
            'CREDITCARD',
            'Invalid Card'
        );
        $request = $client->createRequest();

        $this->assertEquals(json_encode([
            'fromDate' => '2023-01-01',
            'toDate' => '2023-02-02',
            'status' => 'APPROVED',
            'operation' => 'DIRECT',
            'merchantId' => 1,
            'acquirerId' => 2,
            'paymentMethod' => 'CREDITCARD',
            'errorCode' => 'Invalid Card'
        ]), $request->getBody()->getContents());
    }

    public function test_transaction_query_with_invalid_status(): void
    {
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('Invalid status value for Transaction Query Request');
        $client = new TransactionQuery(
            new Carbon('2023-01-01'),
            new Carbon('2023-02-01'),
            "other"
        );
    }

    public function test_transaction_query_with_invalid_operation(): void
    {
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('Invalid operation value for Transaction Query Request');
        $client = new TransactionQuery(
            new Carbon('2023-01-01'),
            new Carbon('2023-02-01'),
            "APPROVED",
            'other'
        );
    }

    public function test_transaction_query_with_invalid_payment_method(): void
    {
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('Invalid payment method value for Transaction Query Request');
        $client = new TransactionQuery(
            new Carbon('2023-01-01'),
            new Carbon('2023-02-01'),
            "APPROVED",
            'DIRECT',
            0,
            0,
            'other'
        );
    }

    public function test_transaction_query_with_invalid_error_code(): void
    {
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('Invalid error code value for Transaction Query Request');
        $client = new TransactionQuery(
            new Carbon('2023-01-01'),
            new Carbon('2023-02-01'),
            "APPROVED",
            'DIRECT',
            0,
            0,
            'CREDITCARD',
            'other'
        );
    }

    public function test_transaction_query_with_invalid_filter_field(): void
    {
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('Invalid filter field for Transaction Query Request');
        $client = new TransactionQuery(
            new Carbon('2023-01-01'),
            new Carbon('2023-02-01'),
            "APPROVED",
            'DIRECT',
            0,
            0,
            'CREDITCARD',
            'Do not honor',
            'other'
        );
    }
}
