<?php

namespace Tests\Unit\Services\Reporting\Endpoints;

use App\Services\Reporting\Endpoints\Client;
use App\Services\Reporting\Endpoints\Endpoint;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function test_client_initialize_class(): void
    {
        $client = new Client('transaction-id');

        $this->assertInstanceOf(Endpoint::class, $client);
    }

    public function test_client_initialize_request(): void
    {
        $client = new Client('transaction-id');
        $request = $client->createRequest();

        $this->assertEquals('client', $request->getUri()->getPath());
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode(["transactionId" => "transaction-id"]), $request->getBody()->getContents());
    }
}
