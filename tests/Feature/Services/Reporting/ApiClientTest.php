<?php

namespace Tests\Feature\Services\Reporting;

use App\Services\Reporting\ApiClient;
use App\Services\Reporting\Endpoints\Client;
use App\Services\Reporting\Endpoints\Responses\ClientResponse;
use App\Services\Reporting\Endpoints\Responses\TransactionResponse;
use App\Services\Reporting\Endpoints\Responses\TransactionQueryResponse;
use App\Services\Reporting\Endpoints\Responses\TransactionReportResponse;
use App\Services\Reporting\Endpoints\Transaction;
use App\Services\Reporting\Endpoints\TransactionQuery;
use App\Services\Reporting\Endpoints\TransactionReport;
use App\Services\Reporting\Response;
use Carbon\Carbon;
use Tests\TestCase;

class ApiClientTest extends TestCase
{
    public function test_api_client_for_successfully_initialized(): void
    {
        $app = $this->createApplication();
        /** @var ApiClient $reportingApiClient */
        $reportingApiClient = $app->make(ApiClient::class);

        $this->assertInstanceOf(ApiClient::class, $reportingApiClient);
    }

    public function test_api_client_with_successful_transactions_report(): void
    {
        $app = $this->createApplication();
        /** @var ApiClient $reportingApiClient */
        $reportingApiClient = $app->make(ApiClient::class);

        $endpoint = new TransactionReport(new Carbon('2015-01-01'), new Carbon('2023-01-01'));
        $response = $reportingApiClient->execute($endpoint);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertNotTrue($response->isFailed());
    }

    public function test_api_client_with_successful_transaction_query(): void
    {
        $app = $this->createApplication();
        /** @var ApiClient $reportingApiClient */
        $reportingApiClient = $app->make(ApiClient::class);

        $endpoint = new TransactionQuery(new Carbon('2023-07-01'), new Carbon('2023-10-01'));
        $response = $reportingApiClient->execute($endpoint);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertNotTrue($response->isFailed());
    }

    public function test_api_client_with_successful_transaction(): void
    {
        $app = $this->createApplication();
        /** @var ApiClient $reportingApiClient */
        $reportingApiClient = $app->make(ApiClient::class);

        $endpoint = new Transaction('1081429-1695032606-3');
        $response = $reportingApiClient->execute($endpoint);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertNotTrue($response->isFailed());
    }

    public function test_api_client_with_successful_client(): void
    {
        $app = $this->createApplication();
        /** @var ApiClient $reportingApiClient */
        $reportingApiClient = $app->make(ApiClient::class);

        $endpoint = new Client('1081429-1695032606-3');
        $response = $reportingApiClient->execute($endpoint);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertNotTrue($response->isFailed());
    }
}
