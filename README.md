## Financial House

This is a sample project to consume the reporting API. Here is the steps to run the project.

Make sure to provide following environment variables : 

```
REPORTING_API_URL=
REPORTING_API_EMAIL=
REPORTING_API_PASSWORD=
```

Run the following command to serve: 

```
php artisan serve
```
