<?php
/**
 * reporting.php
 *
 * @since     Oct 2023
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

return [
    'api' => [
        'url' => env('REPORTING_API_URL'), // Should end with `/` as base_uri
        'email' => env('REPORTING_API_EMAIL'),
        'password' => env('REPORTING_API_PASSWORD'),
    ]
];
