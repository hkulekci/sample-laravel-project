<?php
/**
 * @since     Oct 2023
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

namespace App\Exceptions;

class ValidationException extends \RuntimeException
{

}
