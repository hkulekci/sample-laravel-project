<?php

namespace App\Http\Controllers;

use App\Services\ReportingService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionReportController extends Controller
{
    public function __construct(protected ReportingService $service) {}

    public function __invoke(Request $request)
    {
        $reports = [];

        if ($request->getMethod() === 'POST') {
            $validator = Validator::make($request->all(), [
                'fromDate' => 'date|required',
                'toDate' => 'date|required',
                'merchant' => 'nullable|integer',
                'acquirer' => 'nullable|integer'
            ]);

            $data = $validator->validated();

            $data['fromDate'] = new \Illuminate\Support\Carbon($data['fromDate']);
            $data['toDate'] = new Carbon($data['toDate']);

            $reports = $this->service->getReports($data);
        }

        return view('transaction/report', ['reports' => $reports]);
    }
}
