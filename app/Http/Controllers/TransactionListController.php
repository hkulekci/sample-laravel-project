<?php

namespace App\Http\Controllers;

use App\Services\Reporting\Endpoints\TransactionQuery;
use App\Services\ReportingService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TransactionListController extends Controller
{
    public function __construct(protected ReportingService $service) {}

    public function __invoke(Request $request)
    {
        $transactions = [];

        if ($request->getMethod() === 'POST') {
            $validator = Validator::make($request->all(), [
                'fromDate' => 'date|required',
                'toDate' => 'date|required',
                'status' => [Rule::in(TransactionQuery::VALID_STATUSES), 'nullable'],
                'operation' => [Rule::in(TransactionQuery::VALID_OPERATIONS), 'nullable'],
                'merchantId' => 'nullable|integer',
                'acquirerId' => 'nullable|integer',
                'paymentMethod' => [Rule::in(TransactionQuery::VALID_PAYMENT_METHODS), 'nullable'],
                'errorCode' => [Rule::in(TransactionQuery::VALID_ERROR_CODES), 'nullable'],
                'filterField' => [Rule::in(TransactionQuery::VALID_FILTER_FIELDS), 'nullable'],
                'filterValue' => 'string|nullable',
                'page' => 'nullable|integer',
            ]);

            $data = $validator->validated();

            $data['fromDate'] = new Carbon($data['fromDate']);
            $data['toDate'] = new Carbon($data['toDate']);

            $transactions = $this->service->getTransactions($data);
        }


        return view('transaction/list', [
            'transactions' => $transactions['data'] ?? [],
            'statuses' => TransactionQuery::VALID_STATUSES,
            'operations' => TransactionQuery::VALID_OPERATIONS,
            'paymentMethods' => TransactionQuery::VALID_PAYMENT_METHODS,
            'errorCodes' => TransactionQuery::VALID_ERROR_CODES,
            'filterFields' => TransactionQuery::VALID_FILTER_FIELDS,
        ]);
    }
}
