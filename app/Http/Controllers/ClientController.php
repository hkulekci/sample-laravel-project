<?php

namespace App\Http\Controllers;

use App\Services\ReportingService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct(protected ReportingService $service) {}

    public function __invoke(Request $request, string $id)
    {
        $clientData = $this->service->getClient($id);

        return view('transaction/client', ['client' => $clientData, 'transactionId' => $id]);
    }
}
