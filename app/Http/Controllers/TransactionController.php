<?php

namespace App\Http\Controllers;

use App\Services\ReportingService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function __construct(protected ReportingService $service) {}

    public function __invoke(Request $request, string $id)
    {
        $transaction = $this->service->getTransaction($id);

        return view('transaction/id', ['transaction' => $transaction]);
    }
}
