<?php

namespace App\Providers;

use App\Services\Reporting\ApiClient;
use App\Services\Reporting\Http\Middlewares\Authentication;
use App\Services\Reporting\Http\Middlewares\Retry;
use App\Services\ReportingService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class ReportingServiceProvider extends ServiceProvider
{
    /**
     * Register endpoints.
     */
    public function register()
    {
        $this->app->singleton(ReportingService::class, function (Application $app) {
            return new ReportingService($app->make(ApiClient::class));
        });
    }
}
