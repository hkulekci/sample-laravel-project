<?php
/**
 * @since     Oct 2023
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

namespace App\Services;

use App\Services\Reporting\ApiClient;
use App\Services\Reporting\Endpoints\Client;
use App\Services\Reporting\Response;
use App\Services\Reporting\Endpoints\Transaction;
use App\Services\Reporting\Endpoints\TransactionQuery;
use App\Services\Reporting\Endpoints\TransactionReport;

class ReportingService
{
    public function __construct(protected ApiClient $apiClient) {}

    public function getReports(array $queryParams): Response
    {
        $request = new TransactionReport(...$queryParams);
        return $this->apiClient->execute($request);
    }

    public function getTransactions($queryParams): Response
    {
        $request = new TransactionQuery(...$queryParams);
        return $this->apiClient->execute($request);
    }

    public function getTransaction(string $id): Response
    {
        $request = new Transaction($id);
        return $this->apiClient->execute($request);
    }

    public function getClient(string $id): Response
    {
        $request = new Client($id);
        return $this->apiClient->execute($request);
    }
}
