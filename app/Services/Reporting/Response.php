<?php
/**
 * @since     Nov 2023
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

namespace App\Services\Reporting;

use ArrayAccess;
use Exception;
use Psr\Http\Message\ResponseInterface;

class Response implements ArrayAccess
{
    protected array $raw;
    protected int $status;

    /**
     * @param ResponseInterface $response
     * @throws \JsonException
     */
    public function __construct(protected ResponseInterface $response)
    {
        if ($response->getHeaderLine('content-type') === 'application/json') {
            $this->raw = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        } else {
            $this->raw = [
                'content' => $response->getBody()->getContents()
            ];
        }
        $this->status = $response->getStatusCode();
    }

    public function isFailed()
    {
        if ($this->status >= 200 && $this->status < 300) {
            return false;
        }

        return true;
    }

    public function __toArray(): array
    {
        return $this->raw;
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->raw[$offset]);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->raw[$offset];
    }

    /**
     * @throws Exception
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        throw new Exception('You can not change the response');
    }

    /**
     * @throws Exception
     */
    public function offsetUnset(mixed $offset): void
    {
        throw new Exception('You can not change the response');
    }
}
