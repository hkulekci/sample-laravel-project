<?php

namespace App\Services\Reporting\Http\Middlewares;

use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Middleware;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\RequestInterface;

class Retry
{
    /**
     * Retry requests based on the "decider"
     *
     * @return callable
     */
    public static function create()
    {
        return Middleware::retry(self::class . '::retryDecider', self::class . '::retryDelay');
    }

    /**
     * The conditions on which requests should be retried
     *
     * @param int $retries
     * @param RequestInterface $request
     * @param $response
     * @param $exception
     *
     * @return bool
     */
    public static function retryDecider(
        int $retries,
        RequestInterface $request,
        $response = null,
        $exception = null
    ) : bool {
        // Limit the number of retries
        if ($retries >= 5) {
            return false;
        }

        // Retry connection exceptions
        if ($exception instanceof ConnectException) {
            Log::error('Retry Exception because of connection error', [
                'exception' => $exception,
                'request' => $exception->getRequest(),
            ]);
            return true;
        }

        if ($response) {
            // Retry on server errors
            if ($response->getStatusCode() >= 500 || $response->getStatusCode() === 401) {
                $responseBody = $response->getBody()->getContents();
                Log::error('Retry Exception with '.$response->getStatusCode().' code', [
                    'exception' => $exception,
                    'request' => $exception?->getRequest(),
                    'request_headers' => $request->getHeaders(),
                    'response_content' => $responseBody,
                ]);
                self::invalidTokenCache();
                return true;
            }

            if ($response->getStatusCode() === 400) {
                $responseBody = json_decode($response->getBody()->getContents(), true);
                Log::error('Retry Exception with '.$response->getStatusCode().' code', [
                    'exception' => $exception,
                    'request' => $exception->getRequest(),
                    'request_headers' => $request->getHeaders(),
                    'response_content' => $responseBody,
                ]);

//                if (isset($responseBody['errors']['message'])
//                    && (
//                        Str::contains($responseBody['errors']['message'], 'user-id header')
//                        || Str::contains($responseBody['errors']['message'], 'auth-token header')
//                    )
//                ) {
//                    self::invalidTokenCache();
//                    return true;
//                }
            }
        }

        return false;
    }

    /**
     * The frequency between tried requests
     *
     * @param int $numberOfRetries
     *
     * @return int
     */
    public static function retryDelay(int $numberOfRetries)
    {
        if (getenv('APP_ENV') === 'testing') {
            return 1;
        }

        return (int)pow(2, $numberOfRetries) * 1000;
    }

    public static function invalidTokenCache()
    {
        resolve(CacheRepository::class)->forget('auth_token');
    }
}
