<?php

namespace App\Services\Reporting\Http\Middlewares;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\HttpFactory;
use Illuminate\Cache\Repository;
use Illuminate\Contracts\Cache\Repository as RepositoryContract;
use Psr\Http\Message\RequestInterface;

class Authentication
{
    /**
     * @var Repository
     */
    private $cache;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @param RepositoryContract $cache
     * @param ClientInterface $client
     */
    public function __construct(RepositoryContract $cache, ClientInterface $client)
    {
        $this->cache = $cache;
        $this->client = $client;
    }

    /**
     * Create a Guzzle Middleware
     *
     * @return callable
     */
    public static function create()
    {
        return Middleware::mapRequest(app(Authentication::class));
    }

    /**
     * @param RequestInterface $request
     *
     * @return RequestInterface
     */
    public function __invoke(RequestInterface $request): RequestInterface
    {
        $auth = $this->getAccessToken();
        $token = '';

        if ($auth) {
            $token = $auth['token'];
        }

        return $request
            ->withAddedHeader('Authorization', $token);
    }

    private function getAccessToken()
    {
        return $this->cache->remember('reporting_service_api_auth_token', 600, function () {
            $uri = config()->get('reporting.api.url') . 'merchant/user/login';

            $body = [
                'email' => config()->get('reporting.api.email'),
                'password' => config()->get('reporting.api.password'),
            ];

            $httpFactory = new HttpFactory();
            $request = $httpFactory->createRequest('POST', $uri);
            $request = $request->withBody(
                    $httpFactory->createStream(json_encode($body, JSON_THROW_ON_ERROR))
                )
                ->withHeader('accept', 'application/json')
                ->withHeader('content-type', 'application/json');


            $response = $this->client->send($request);

            if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
                $responseBody = $response->getBody();
                return json_decode($responseBody->getContents(), JSON_OBJECT_AS_ARRAY, 512, JSON_THROW_ON_ERROR);
            }
            return false;
        });
    }
}
