<?php

namespace App\Services\Reporting;

use App\Services\Reporting\Http\Middlewares\Authentication;
use App\Services\Reporting\Http\Middlewares\Retry;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\ServiceProvider;

class ApiClientServiceProvider extends ServiceProvider
{
    /**
     * Register endpoints.
     */
    public function register()
    {
        $this->app->singleton(ClientInterface::class, function () {
            return new Client();
        });

        $this->app->singleton(ApiClient::class, static function () {
            $stack = HandlerStack::create();
            $stack->push(Retry::create());
            $stack->push(Authentication::create());
            return (new ApiClient(new Client([
                'base_uri' => config('reporting.api.url'),
                'headers' => [
                    'content-type' => 'application/json',
                    'accept' => 'application/json',
                    'user-agent' => config('app.name'),
                ],
                'handler' => $stack,
            ])));
        });

        $this->app->bind(Authentication::class, function () {
            return new Authentication(
                $this->app->make(Repository::class),
                $this->app->make(ClientInterface::class)
            );
        });
    }
}
