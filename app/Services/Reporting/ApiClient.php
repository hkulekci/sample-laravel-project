<?php
/**
 * @since     Oct 2023
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

namespace App\Services\Reporting;

use App\Services\Reporting\Endpoints\Endpoint;
use Psr\Http\Client\ClientInterface;

class ApiClient implements Client
{
    private ClientInterface $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function execute(Endpoint $endpoint): Response
    {
        $response = $this->client->sendRequest($endpoint->createRequest());

        return new Response($response);
    }
}
