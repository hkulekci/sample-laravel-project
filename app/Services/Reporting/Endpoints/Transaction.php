<?php
/**
 * TransactionReport
 *
 * @since     Oct 2023
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

namespace App\Services\Reporting\Endpoints;

use GuzzleHttp\Psr7\HttpFactory;
use Psr\Http\Message\RequestInterface;

class Transaction implements Endpoint
{
    public function __construct(
        protected string $transactionId
    ) {}

    public function getPath(): string
    {
        return 'transaction';
    }

    public function createRequest(): RequestInterface
    {
        $httpFactory = new HttpFactory();
        $request = $httpFactory->createRequest('POST', $this->getPath());
        $body = [
            'transactionId' => $this->transactionId,
        ];

        return $request->withBody(
            $httpFactory->createStream(json_encode($body, JSON_THROW_ON_ERROR))
        );
    }
}
