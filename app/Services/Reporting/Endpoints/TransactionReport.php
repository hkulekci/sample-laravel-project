<?php
/**
 * TransactionReport
 *
 * @since     Oct 2023
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

namespace App\Services\Reporting\Endpoints;

use Carbon\Carbon;
use GuzzleHttp\Psr7\HttpFactory;
use Psr\Http\Message\RequestInterface;

class TransactionReport implements Endpoint
{
    public function __construct(
        protected Carbon $fromDate,
        protected Carbon $toDate,
        protected ?int $merchant = null,
        protected ?int $acquirer = null
    ) {}

    public function getPath(): string
    {
        return 'transactions/report';
    }

    public function createRequest(): RequestInterface
    {
        $httpFactory = new HttpFactory();
        $request = $httpFactory->createRequest('POST', $this->getPath());
        $body = [
            'fromDate' => $this->fromDate->format('Y-m-d'),
            'toDate' => $this->toDate->format('Y-m-d')
        ];
        if ($this->merchant) {
            $body['merchant'] = $this->merchant;
        }
        if ($this->acquirer) {
            $body['acquirer'] = $this->acquirer;
        }

        return $request->withBody(
            $httpFactory->createStream(json_encode($body, JSON_THROW_ON_ERROR))
        );
    }
}
