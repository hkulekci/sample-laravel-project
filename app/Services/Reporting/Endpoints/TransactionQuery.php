<?php
/**
 * TransactionReport
 *
 * @since     Oct 2023
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

namespace App\Services\Reporting\Endpoints;

use App\Exceptions\ValidationException;
use Carbon\Carbon;
use GuzzleHttp\Psr7\HttpFactory;
use Psr\Http\Message\RequestInterface;

class TransactionQuery implements Endpoint
{
    public const VALID_STATUSES = ["APPROVED", "WAITING", "DECLINED", "ERROR"];
    public const VALID_OPERATIONS = ["DIRECT", "REFUND", "3D", "3DAUTH", "STORED"];
    public const VALID_PAYMENT_METHODS = ["CREDITCARD", "CUP", "IDEAL", "GIROPAY", "MISTERCASH", "STORED", "PAYTOCARD", "CEPBANK", "CITADEL" ];
    public const VALID_ERROR_CODES = [
        "Do not honor",
        "Invalid Transaction",
        "Invalid Card",
        "Not sufficient funds",
        "Incorrect PIN",
        "Invalid country association",
        "Currency not allowed",
        "3-D Secure Transport Error",
        "Transaction not permitted to cardholder"
    ];
    public const VALID_FILTER_FIELDS = ["Transaction UUID", "Customer Email", "Reference No", "Custom Data", "Card PAN"];

    public function __construct(
        protected ?Carbon $fromDate = null,
        protected ?Carbon $toDate = null,
        protected ?string $status = null,
        protected ?string $operation = null,
        protected ?int $merchantId = null,
        protected ?int $acquirerId = null,
        protected ?string $paymentMethod  = null,
        protected ?string $errorCode = null,
        protected ?string $filterField = null,
        protected ?string $filterValue = null,
        protected ?int $page = null
    ) {
        if ($this->status && !in_array($this->status, self::VALID_STATUSES, true)) {
            throw new ValidationException('Invalid status value for Transaction Query Request');
        }
        if ($this->operation && !in_array($this->operation, self::VALID_OPERATIONS, true)) {
            throw new ValidationException('Invalid operation value for Transaction Query Request');
        }
        if ($this->paymentMethod && !in_array($this->paymentMethod, self::VALID_PAYMENT_METHODS, true)) {
            throw new ValidationException('Invalid payment method value for Transaction Query Request');
        }
        if ($this->errorCode && !in_array($this->errorCode, self::VALID_ERROR_CODES, true)) {
            throw new ValidationException('Invalid error code value for Transaction Query Request');
        }
        if ($this->filterField && !in_array($this->filterField, self::VALID_FILTER_FIELDS, true)) {
            throw new ValidationException('Invalid filter field for Transaction Query Request');
        }
    }

    public function getPath(): string
    {
        return 'transaction/list';
    }

    public function createRequest(): RequestInterface
    {
        $httpFactory = new HttpFactory();
        $request = $httpFactory->createRequest('POST', $this->getPath());
        $body = [
            'fromDate' => $this->fromDate?->format('Y-m-d'),
            'toDate' => $this->toDate?->format('Y-m-d'),
            'status' => $this->status,
            'operation' => $this->operation,
            'merchantId' => $this->merchantId,
            'acquirerId' => $this->acquirerId,
            'paymentMethod' => $this->paymentMethod,
            'errorCode' => $this->errorCode,
            'filterField' => $this->filterField,
            'filterValue' => $this->filterValue,
            'page' => $this->page,
        ];

        $body = array_filter($body);

        if ($body) {
            return $request->withBody(
                $httpFactory->createStream(json_encode($body, JSON_THROW_ON_ERROR))
            );
        }

        return $request;
    }
}
