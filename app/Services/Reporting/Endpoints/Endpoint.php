<?php
/**
 * Enpoint.php
 *
 * @since     Oct 2023
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

namespace App\Services\Reporting\Endpoints;

use App\Services\Reporting\Endpoints\Responses\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface Endpoint
{
    public function getPath(): string;

    public function createRequest(): RequestInterface;
}
