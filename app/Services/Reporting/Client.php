<?php
/**
 * Crawler.php
 *
 * @since     Sep 2023
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

namespace App\Services\Reporting;

use App\Services\Reporting\Endpoints\Endpoint;

interface Client
{
    public function execute(Endpoint $endpoint): Response;
}
